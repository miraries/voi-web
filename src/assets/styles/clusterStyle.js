const clusterStyle = [
    {
        textColor: "white",
        url: process.env.BASE_URL + "/img/icons/cluster/m1.png",
        anchorText: [-5, 0],
        height: 46,
        width: 40,
        textSize: 15
    },
    {
        textColor: "white",
        url: process.env.BASE_URL + "/img/icons/cluster/m2.png",
        anchorText: [-5, 0],
        height: 50,
        width: 43,
        textSize: 15
    },
    {
        textColor: "white",
        url: process.env.BASE_URL + "/img/icons/cluster/m3.png",
        anchorText: [-5, 0],
        height: 55,
        width: 47,
        textSize: 15
    },
    {
        textColor: "white",
        url: process.env.BASE_URL + "/img/icons/cluster/m4.png",
        anchorText: [-5, 0],
        height: 58,
        width: 50,
        textSize: 15
    },
    {
        textColor: "white",
        url: process.env.BASE_URL + "/img/icons/cluster/m5.png",
        anchorText: [-5, 0],
        height: 62,
        width: 53,
        textSize: 15
    }
];
export default clusterStyle;