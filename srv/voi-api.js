const got = require("got");
const faker = require("faker");
const debug = require('debug')('voiapi');
const geolib = require('geolib');

class VoiAPI {
    constructor() {
        if (!process.env.VOI_AUTHENTICATION_TOKEN) {
            console.log('Authentication token not provided in environment')
            process.exit();
        }

        this.token = process.env.VOI_ACCESS_TOKEN || '';
        this.authToken = process.env.VOI_AUTHENTICATION_TOKEN;

        debug(`Instantiated voi api with access token ${this.token}`);
    }

    async acquireToken() {
        const response = await got.post("https://api.voiapp.io/v1/auth/session", {
            timeout: 5000,
            json: true,
            headers: {
                appversion: "2.27.1",
                OS: "Android",
                model: "SM-G955N",
                brand: "Android",
                manufacturer: "samsung",
                Connection: "Keep-Alive",
                "X-Access-Token": this.token,
                "X-Request-Id": faker.random.uuid(),
                "App-Name": "Rider",
                "Content-Type": "application/json; charset=UTF-8",
                "Accept-Encoding": "gzip",
                "User-Agent": "okhttp/3.12.1"
            },
            body: JSON.stringify({
                authenticationToken: this.authToken
            })
        });

        debug(`Acquiring new token, status code ${response.statusCode}`);
        debug(`New token ${response.body.accessToken}`);

        this.token = response.body.accessToken;
    }

    async getVehicles(zone, status) {
        if (!['ready', 'riding', 'sleep', 'bounty', 'home', 'collected', 'lost'].includes(status))
            throw 'Invalid status'

        try {
            const response = await got(
                `https://api.voiapp.io/v1/vehicles/zone/${zone}/${status}`,
                {
                    timeout: 5000,
                    json: true,
                    headers: {
                        appversion: "2.27.1",
                        OS: "Android",
                        model: "SM-G955N",
                        brand: "Android",
                        manufacturer: "samsung",
                        Connection: "Keep-Alive",
                        "X-Access-Token": this.token,
                        "X-Request-Id": faker.random.uuid(),
                        "App-Name": "Rider",
                        "Content-Type": "application/json; charset=UTF-8",
                        "Accept-Encoding": "gzip",
                        "User-Agent": "okhttp/3.12.1"
                    }
                }
            );

            debug(`Fetching vehicles, status code ${response.statusCode}`);

            return response.body;
        } catch (error) {
            debug(`Error fethcing vehicles, status code ${error.statusCode}, error ${error}`);

            if (error.response.body.code === '401.2') {
                debug(`Acquiring new token and refetching vehicles`);
                await this.acquireToken().catch(() => { throw 'Error acquiring new token' });
                return await this.getVehicles();
            }
        }
    }

    async getZones() {
        try {
            const response = await got(
                `https://api.voiapp.io/v1/zones`,
                {
                    timeout: 5000,
                    json: true,
                    headers: {
                        appversion: "2.27.1",
                        OS: "Android",
                        model: "SM-G955N",
                        brand: "Android",
                        manufacturer: "samsung",
                        Connection: "Keep-Alive",
                        "X-Access-Token": this.token,
                        "X-Request-Id": faker.random.uuid(),
                        "App-Name": "Rider",
                        "Content-Type": "application/json; charset=UTF-8",
                        "Accept-Encoding": "gzip",
                        "User-Agent": "okhttp/3.12.1"
                    }
                }
            );

            debug(`Fetching zones, status code ${response.statusCode}`);

            return response.body.zones;
        } catch (error) {
            debug(`Error fethcing zones, status code ${error.statusCode}, error ${error}`);

            if (error.response.body.code === '401.2') {
                debug(`Acquiring new token and refetching zones`);
                await this.acquireToken().catch(() => {throw 'Error acquiring new token'});
                return await this.getZones();
            }
        }
    }

    async getClosestDistance(userLocation = { latitude: 59.6248714, longitude: 16.5569127 }) {
        const vehicles = await this.getVehicles(171, 'ready');
        const locations = vehicles.map(v => {
            let loc = { latitude: v.location[0], longitude: v.location[1] }
            return loc;
        });

        const closestVehicle = geolib.findNearest(userLocation, locations);

        const distance = geolib.getDistance(userLocation, closestVehicle)

        return { closestVehicle, distance };
    }
}

module.exports = {
    VoiAPI
};
