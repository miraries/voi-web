import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import vuetify from './plugins/vuetify';

import * as VueGoogleMaps from 'vue2-google-maps'

import GmapCluster from "vue2-google-maps/dist/components/cluster";
Vue.component('GmapCluster', GmapCluster);

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.VUE_APP_GMAPS_API_KEY,
  },
  installComponents: true
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
