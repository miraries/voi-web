module.exports = {
  publicPath: process.env.BASE_URL,
  pluginOptions: {
    express: {
      shouldServeApp: process.env.SHOULD_EXPRESS_SERVE_APP === undefined ?
        process.env.NODE_ENV !== 'production' : process.env.SHOULD_EXPRESS_SERVE_APP,
      serverDir: './srv'
    }
  },
  pwa: {
    themeColor: "#ec6859"
  }
}
