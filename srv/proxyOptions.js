import uuid from 'uuid/v4';
const debug = require('debug')('proxy:paths');

export default (proxyReqOpts) => {
    proxyReqOpts.headers['X-App-Version'] = '3.4.2';
    proxyReqOpts.headers['X-OS'] = 'Android';
    proxyReqOpts.headers['X-OS-Version'] = '24';
    proxyReqOpts.headers['model'] = 'LGMP260';
    proxyReqOpts.headers['brand'] = 'MetroPCS';
    proxyReqOpts.headers['manufacturer'] = 'LGE';
    proxyReqOpts.headers['X-App-Name'] = 'Rider';
    proxyReqOpts.headers['X-Request-Id'] = uuid();
    proxyReqOpts.headers['User-Agent'] = 'okhttp/3.12.1';

    delete proxyReqOpts.headers['x-forwarded-host'];
    delete proxyReqOpts.headers['x-forwarded-proto'];
    delete proxyReqOpts.headers['x-forwarded-port'];
    delete proxyReqOpts.headers['x-forwarded-for'];
    delete proxyReqOpts.headers['x-powered-by'];
    delete proxyReqOpts.headers['accept-language'];
    delete proxyReqOpts.headers['user-agent'];
    delete proxyReqOpts.headers['cookie'];
    delete proxyReqOpts.headers['referer'];
    delete proxyReqOpts.headers['origin'];

    debug(`Proxied path: ${proxyReqOpts.path}`);

    return proxyReqOpts;
}