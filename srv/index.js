require("dotenv").config();
import express from 'express';
import apicache from 'apicache';
import { VoiAPI } from './voi-api';
import proxy from 'express-http-proxy';
import proxyReqOptDecorator from './proxyOptions';
import 'express-async-errors';

let cache = apicache.options({
  headers: {
    'cache-control': 'no-cache',
  },
}).middleware

const cacheSuccesses = cache('10 minutes', (req, res) => res.statusCode === 200)

const voiApi = new VoiAPI();

export default (app, http) => {
  app.use(express.json());

  app.use('/api/proxy', proxy('https://api.voiapp.io/v1/', { proxyReqOptDecorator }));

  app.get('/api/healthcheck', (req, res) => {
    res.json({ ok: true });
  });

  app.get('/api/:zone/vehicles/riding', async (req, res) => {
    res.json(await voiApi.getVehicles(req.params.zone, 'riding'));
  });

  app.get('/api/:zone/vehicles/:status', cacheSuccesses, async (req, res) => {
    res.json(await voiApi.getVehicles(req.params.zone, req.params.status));
  });

  app.get('/api/zones', cache('2 hours'), async (req, res) => {
    res.json(await voiApi.getZones());
  });

  app.get('/api/closest', cacheSuccesses, async (req, res) => {
    res.json(await voiApi.getClosestDistance());
  });

  app.use((err, req, res, next) => {
    res.status(500).json({ error: err });
  });
}
