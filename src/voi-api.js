import axios from 'axios'

const createInstance = function (token) {
    return axios.create({
        baseURL: process.env.BASE_URL + 'api/proxy/v1/',
        timeout: 3000,
        validateStatus: () => {
            return true // Reject only if the status code is greater than or equal to 500
        },
        headers: {
            'X-Access-Token': token
        }
    });
}

const getLocation = async function () {
    if (!navigator.geolocation)
        throw 'Cannot fetch location'

    const position = await new Promise(function (resolve, reject) {
        navigator.geolocation.getCurrentPosition(resolve, reject);
    });

    return {
        lat: position.coords.latitude,
        lng: position.coords.longitude
    }
}

export default {
    request(token, request, params) {
        const instance = createInstance(token)

        // The token changes on the fly so a new instance is generated every time 
        // and the request passed as a callback, so the token does not have to be
        // injected into every request separately, only the instance
        return request(instance, params)
    },
    async refreshState(instance) {
        const [payments, rides, wallet] = await Promise.all([
            instance.get("psp/payments/default"),
            instance.get("rides/current"),
            instance.get("users/wallet")
        ]);

        const hasError = [payments, rides, wallet].find(x => x.status < 200 || x.status >= 300)
        if (hasError)
            throw hasError.data ? hasError.data.message : hasError.statusText


        return {
            payment_method: payments.data ? payments.data.provider : 'Unknown',
            credits: wallet.data ? wallet.data.voiCreditsBalance : 'Unknown',
            currentRide: rides.data ? {
                vehicleShort: rides.data.vehicleShort,
                startTime: rides.data.startTime
            } : null
        }
    },
    async startRide(instance, { vehicleCode, device_token }) {
        const precheck = await instance.get(`rides/precheck/${vehicleCode}`)
        if (!precheck.data || !precheck.data.vehicle || precheck.data.vehicle.status !== 'ready')
            throw 'Cannot start ride: Failed precheck'

        const start = await instance.post(`rides/start/${precheck.data.vehicle.id}`, { device_token })
        if (start.status !== 201) throw 'Cannot start ride - Failed start'

        const unlock = await instance.post('rides/unlock')
        if (unlock.status !== 200) throw 'Cannot start ride - Failed unlock'
    },
    async lockRide(instance) {
        const lock = await instance.post("rides/lock")
        if (lock.status !== 200) throw 'Cannot lock ride'
    },
    async unlockRide(instance) {
        const unlock = await instance.post("rides/unlock")
        if (unlock.status !== 200) throw 'Cannot unlock ride'
    },
    async endRide(instance) {
        const end = await instance.post("rides/end_session", { location: await getLocation() })
        if (end.status !== 200) throw 'Cannot end ride'
    }
}