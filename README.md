<h1>Welcome to voi-web 👋</h1>

<img alt="Project banner" src="docs/banner.jpg">

<div>
<p>
<img alt="Version"  src="https://img.shields.io/badge/version-0.4.1-blue.svg?cacheSeconds=2592000" />

<!-- <a href="gitlab.com/miraries/voi-web/-/wikis"  target="_blank">
<img alt="Documentation"  src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
</a> -->

<a href="" target="_blank">
<img  alt="Documentation"  src="https://gitlab.com/miraries/voi-web/badges/master/pipeline.svg" />
</a>

<a href="gitlab.com/miraries/voi-web/-/blob/master/LICENSE"  target="_blank">
<img alt="License: GNU GPLv3"  src="https://img.shields.io/badge/License-GNU GPLv3-yellow.svg" />
</a>
</p>
</div>

> Exploring and visualizing undocumented and unused API endpoints of the VOI Scooter backend for the Android app. In addition an API explorer available (like Swagger UI), as well as a client supporting multiple accounts at one time. Endpoints were found by reverse-engineering the app, mitm-ing, as well as outright guessing.

### ✨ [Live Demo](https://ivnk.dev/voi-web)

## Notable tech used

-   Vue
-   Vuex
-   Vuetify
-   vue-cli-plugin-express
-   vue2-google-maps
-   axios
-   got
-   apicache
-   semantic-release

## Features

-   Decoupled Voi API from Express API, automatic token reacquisition
-   API response caching, with optional cache busting
-   Allows for sending requests from browser without limitations (CORS) by proxying through express
-   Map showing scooter locations with filtering and clustering
-   Map showing real time scooter locations
-   API Explorer (Swagger UI like interface)
-   Multi-client - allows for using multiple accounts at a time
-   Dark mode
-   Semantic versioning, CI/CD

## Usage

Copy `.env.example` to `.env` and insert adequate tokens.

To start your server for development purpose, use this command:

```sh
yarn express
```

The server will be automatically restarted when a change is detected.

You just then have to start the app:

```sh
yarn serve
```

To run the server only once for production use, run:

```sh
yarn express:run
```

Updating vue-cli-plugin-express will update the Express server service 👍

To enable/disable debug messages set or remove in env: `DEBUG=voiapi`

## Usage - Docker

To work out-of-the-box set env: `SHOULD_EXPRESS_SERVE_APP=true`

### Build container

```sh
docker build -t miraries/voi-web .
```

### Then run it

```sh
docker run -p 3000:3000 --env-file .env miraries/voi-web
```

## Author

👤 **Ivan Kotlaja**

-   Website: [ivnk.dev](https://ivnk.dev)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

This project uses the [ESLint Convention](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-eslint) for commit messages to allow for automated semantic releases.

## 📝 License

Copyright © 2020 [Ivan Kotlaja](https://github.com/miraries).<br />

This project is [GNU GPLv3](gitlab.com/miraries/voi-web/-/blob/master/LICENSE) licensed.
