import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/vehicle-list',
      name: 'vehicles-list',
      component: () => import(/* webpackChunkName: "vehicle-list" */ './views/VehicleList.vue')
    },
    {
      path: '/vehicle-map',
      name: 'vehicles-map',
      component: () => import(/* webpackChunkName: "vehicle-zone" */ './views/VehicleMap.vue')
    },
    {
      path: '/vehicle-riding-map',
      name: 'vehicles-riding-map',
      component: () => import(/* webpackChunkName: "vehicle-zone" */ './views/VehicleRidingMap.vue')
    },
    {
      path: '/zone-list',
      name: 'zone-list',
      component: () => import(/* webpackChunkName: "vehicle-zone" */ './views/ZoneList.vue')
    },
    {
      path: '/api-explorer',
      name: 'api-explorer',
      component: () => import(/* webpackChunkName: "api-explorer" */ './views/ApiExplorer.vue')
    },
    {
      path: '/multi-client',
      name: 'multi-client',
      component: () => import(/* webpackChunkName: "multi-client" */ './views/MultiClient.vue')
    }
  ]
})
