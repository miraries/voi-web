const mapStyle = {
    styles: [
        {
            "featureType": "all",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        }
    ]
}

export default mapStyle;