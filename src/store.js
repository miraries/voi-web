import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import axios from "@/plugins/axios"
import voiApi from "./voi-api"

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  state: {
    api_responses: [],
    tokens: {
      access_token: '',
      authentication_token: '',
      device_token: ''
    },
    client: {
      users: [],
      vehicles: []
    },
    global_snackbar: {
      type: '',
      text: '',
      visible: false
    }
  },
  getters: {
    getUserByEmail: (state) => (email) => {
      return state.client.users.find(u => u.email === email)
    }
  },
  mutations: {
    addResponse(state, response) {
      state.api_responses.push(response)
    },
    clearResponses(state) {
      state.api_responses = []
    },
    setAccessToken(state, token) {
      state.tokens.access_token = token
    },
    setAuthenticationsToken(state, token) {
      state.tokens.authentication_token = token
    },
    setDeviceToken(state, token) {
      state.tokens.device_token = token;
    },
    addClientUser(state, user) {
      state.client.users.push(user)
    },
    removeClientUser(state, user) {
      state.client.users = state.client.users.filter(u => u.email !== user.email)
    },
    editClientUser(state, user) {
      state.client.users = [...state.client.users.map(u => u.email !== user.email ? u : { ...u, ...user })]
    },
    updateClientReadyVehicles(state, vehicles) {
      state.client.vehicles = vehicles
    },
    setGlobalSnackbar(state, { type, text }) {
      state.global_snackbar = { type, text };
      state.global_snackbar.visible = true;
    }
  },
  actions: {
    addResponse({ commit }, response) {
      commit('addResponse', response)
    },
    setAccessToken({ commit }, token) {
      commit('setAccessToken', token)
    },
    setAuthenticationsToken({ commit }, token) {
      commit('setAuthenticationsToken', token)
    },
    setDeviceToken({ commit }, token) {
      commit('setDeviceToken', token)
    },
    clearResponses({ commit }) {
      commit('clearResponses')
    },
    addClientUser({ commit }, user) {
      commit('addClientUser', user)
    },
    removeClientUser({ commit }, user) {
      commit('removeClientUser', user)
    },
    editClientUser({ commit }, user) {
      commit('editClientUser', user)
    },
    async refreshClientVehicles({ commit }, zone) {
      const { data } = await axios.get(`/api/${zone}/vehicles/ready`)
      const vehicles = data.map(v => ({ short: v.short, battery: v.battery }))
      commit('updateClientReadyVehicles', vehicles)
    },
    async refreshClientUser({ commit }, user) {
      const newData = await voiApi.request(user.authentication_token, voiApi.refreshState)
      commit('editClientUser', { ...user, ...newData })
    },
    async refreshAllClientUser({ state, commit }) {
      state.client.users.map(user => commit('refreshClientUser', user))
    }
  },
  plugins: [vuexLocal.plugin]
})
