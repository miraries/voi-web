/* eslint-disable no-console */

import { register } from 'register-service-worker'
import store from './store'

if (process.env.NODE_ENV === 'production') {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready() {
      store.commit('setGlobalSnackbar', {
        type: 'info', text:
          'App is being served from cache by a service worker.\n' +
          'For more details, visit https://goo.gl/AFskqB'
      })
    },
    registered() {
      store.commit('setGlobalSnackbar', { type: 'info', text: 'Service worker has been registered.' })
    },
    cached() {
      store.commit('setGlobalSnackbar', { type: 'info', text: 'Content has been cached for offline use.' })
    },
    updatefound() {
      store.commit('setGlobalSnackbar', { type: 'info', text: 'New content is downloading.' })
    },
    updated() {
      store.commit('setGlobalSnackbar', { type: 'info', text: 'New content is available; please refresh.' })
    },
    offline() {
      store.commit('setGlobalSnackbar', { type: 'info', text: 'No internet connection found. App is running in offline mode.' })
    },
    error(error) {
      store.commit('setGlobalSnackbar', { type: 'error', text: 'Error during service worker registration: ' + error })
    }
  })
}
