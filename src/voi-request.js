import axios from 'axios'
import store from './store'

const instance = axios.create({
    baseURL: process.env.BASE_URL + 'api/proxy/v1/',
    timeout: 3000,
    validateStatus: () => {
        return true // Reject only if the status code is greater than or equal to 500
    }
});

instance.interceptors.request.use(config => {
    config.headers['X-Access-Token'] = store.state.tokens.authentication_token;
    return config;
})

export default instance