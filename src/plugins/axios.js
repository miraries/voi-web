import axios from "axios";
import store from '../store'

let axiosInstance = axios.create({
    baseURL: process.env.BASE_URL,
    timeout: 5000
});

axiosInstance.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    store.commit('setGlobalSnackbar', {
        type: 'error', text: error.response.data ? error.response.data.error : error.message
    })
    return Promise.reject(error);
});

export default axiosInstance;